from django.shortcuts import render, get_object_or_404, redirect
from django.views.decorators.csrf import csrf_exempt
from xml.sax.handler import ContentHandler
from xml.sax import make_parser

from .apps import xmlStream
from .models import VideoYoutube

# Create your views here.

parserDone = False

@csrf_exempt
def ytvideos(request):

    class YTHandler(ContentHandler):
        def __init__ (self):
            self.inEntry = False
            self.inContent = False
            self.content = ""
            self.title = ""
            self.link = ""
            self.videoId = ""
            self.thumbnail = ""
            self.uri = ""
            self.name = ""
            self.description = ""
            self.published = ""

        def startElement (self, name, attrs):
            if name == 'entry':
                self.inEntry = True
            elif self.inEntry:
                if name == 'title':
                    self.inContent = True
                elif name == 'link':
                    self.link = attrs.get('href')
                elif name == 'yt:videoId':
                    self.inContent = True
                elif name == 'media:thumbnail':
                    self.thumbnail = attrs.get('url')
                elif name == 'uri':
                    self.inContent = True
                elif name == 'name':
                    self.inContent = True
                elif name == 'media:description':
                    self.inContent = True
                elif name == 'published':
                    self.inContent = True

        def endElement (self, name):
            global videos
            if name == 'entry':
                self.inEntry = False
                try:
                    video = VideoYoutube.objects.get(link = self.link) 
                except:
                    video = VideoYoutube(name = self.title, link = self.link, videoId = self.videoId, thumbnail = self.thumbnail, uri = self.uri, channel = self.name, description = self.description, published = self.published, select = False)
                    video.save()
                
            elif self.inEntry:
                if name == 'title':
                    self.title = self.content
                elif name == 'yt:videoId':
                    self.videoId = self.content
                elif name == 'uri':
                    self.uri = self.content
                elif name == 'name':
                    self.name = self.content
                elif name == 'media:description':
                    self.description = self.content
                elif name == 'published':
                    self.published = self.content

                self.content = ""
                self.inContent = False

        def characters (self, chars):
            if self.inContent:
                self.content = self.content + chars

    global parserDone
    if (not parserDone):
        # Load parser and driver
        Parser = make_parser()
        Parser.setContentHandler(YTHandler())
        Parser.parse(xmlStream)
        parserDone = True

    if request.method == "POST":
        name = request.POST['name']
        video = VideoYoutube.objects.get(name = name)
        if video.select == False:
            video.select = True
        elif video.select == True:
            video.select = False
        video.save()

    selectable_list = VideoYoutube.objects.filter(select = False)
    selection_list = VideoYoutube.objects.filter(select = True)
    context = {'selectable_list': selectable_list, 'selection_list': selection_list}
    return render(request, 'youtube/main.html', context)

@csrf_exempt
def identifier(request, videoId):
    if request.method == "GET":
        try:
            video = VideoYoutube.objects.get(videoId = videoId) 
            if video.select == True:
                context = {'video': video}
                return render(request, 'youtube/video.html', context)
            else:
                 raise 
        except:
            context = {'videoId': videoId}
            return render(request, 'youtube/error.html', context)
            
