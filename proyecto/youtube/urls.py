from . import views
from django.urls import path

urlpatterns = [
    path('', views.ytvideos),
    path('<str:videoId>', views.identifier),
]
